CC = g++ -std=c++11
#DEBUG = -ggdb -O0 -Wall -Wextra
CFLAGS = -O3 -Wall -Wextra

# Check if we are using Mac OS(Dorwin) or linux
# and link in the GLUT and openGL libraries in
# the appropriate manner.
ifeq ($(shell uname),Darwin)
	LIBS = -framework OpenGL -framework GLUT -Wno-deprecated
else
	LIBS = -lglut -lGLU -lGL
endif

# Output program name
PANIM = probeAnim
NANIM = probeData
# Support files to main collapse routine
SUPPORT =  IO.cpp render.cpp
ANIM = 2DsolverAnim.cpp
NNIM = 2Dsolver.cpp
HEADERS = 2Dsolver.hpp
all: $(PANIM) $(NANIM)
# Compile code
$(PANIM): $(SUPPORT) $(HEADERS) $(ANIM) $(NNIM)
	$(CC) $(CFLAGS) $(DEBUG) $(ANIM) $(SUPPORT) $(LIBS) -o $(PANIM)
$(NANIM): $(SUPPORT) $(HEADERS) $(NNIM)
	$(CC) $(CFLAGS) $(DEBUG) $(NNIM) $(SUPPORT) $(LIBS) -o $(NANIM)


clean:
	rm $(PANIM) $(NANIM)
check-syntax:
	$(CC) -o nul -S ${CHK_SOURCES}
