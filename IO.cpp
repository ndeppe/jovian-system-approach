#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <math.h>
// Include our header file
#include "2Dsolver.hpp"

void writeDataText() {
  // Output file name
  char fileName[180];
  sprintf(fileName, "test.tsv");
  // Open output file
  std::ofstream outFile;
  outFile.open(fileName);

  // create header
  outFile << "time \t x_J \t y_J \t dx_J \t dy_J \t"
          << "ddx_J \t ddy_J \t"
          << "x_Io \t y_Io \t dx_Io \t dy_Io \t"
          << "ddx_Io \t ddy_Io \t"
          << "x_sat \t y_sat \t dx_sat \t dy_sat \t"
          << "ddx_sat \t ddy_sat \n";
  for (int ii = 0; ii < nt; ++ii) {
    // Print time value
    outFile << ii* dt << "\t";
    // Print Jupiter values
    outFile << jupiter[ii][0][0] << "\t" << jupiter[ii][0][1] << "\t"
            << jupiter[ii][1][0] << "\t" << jupiter[ii][1][1] << "\t"
            << jupiter[ii][2][0] << "\t" << jupiter[ii][2][1] << "\t";
    outFile << moonIo[ii][0][0] << "\t" << moonIo[ii][0][1] << "\t"
            << moonIo[ii][1][0] << "\t" << moonIo[ii][1][1] << "\t"
            << moonIo[ii][2][0] << "\t" << moonIo[ii][2][1] << "\t";
    outFile << sat[ii][0][0] << "\t" << sat[ii][0][1] << "\t" << sat[ii][1][0]
            << "\t" << sat[ii][1][1] << "\t" << sat[ii][2][0] << "\t"
            << sat[ii][2][1] << "\n";
    // Print radius
    // outFile << sqrt(pow(moonIo[ii][0][0]-jupiter[ii][0][0],2)+
    // 		    pow(moonIo[ii][0][1]-jupiter[ii][0][1],2))
    // 	    << "\n";
  }
  outFile.close();
  return;
}

void writeDataBin() {
  double* xval = new double[nt];
  double* yval = new double[nt];
  // Output file name
  char fileName[180];
  sprintf(fileName, "moonIo.bin");
  FILE* outFile;
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = moonIo[ii][0][0];
    yval[ii] = moonIo[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);

  // Write Satellite
  memset(fileName, 0, sizeof(fileName));
  sprintf(fileName, "satellite.bin");
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = sat[ii][0][0];
    yval[ii] = sat[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);

  // Write Ganymede
  memset(fileName, 0, sizeof(fileName));
  sprintf(fileName, "moonGan.bin");
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = moonGan[ii][0][0];
    yval[ii] = moonGan[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);

  // Write Io
  memset(fileName, 0, sizeof(fileName));
  sprintf(fileName, "moonIo.bin");
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = moonIo[ii][0][0];
    yval[ii] = moonIo[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);

  // Write Callisto
  memset(fileName, 0, sizeof(fileName));
  sprintf(fileName, "moonCal.bin");
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = moonCal[ii][0][0];
    yval[ii] = moonCal[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);

  // Write Europa
  memset(fileName, 0, sizeof(fileName));
  sprintf(fileName, "moonEu.bin");
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = moonEu[ii][0][0];
    yval[ii] = moonEu[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);

  // Write Jupiter
  memset(fileName, 0, sizeof(fileName));
  sprintf(fileName, "jupiter.bin");
  outFile = fopen(fileName, "wb");
  if (outFile == NULL) {
    printf("Unable to write output file!\t%s", fileName);
    return;
  }
  for (int ii = 0; ii < nt; ++ii) {
    xval[ii] = jupiter[ii][0][0];
    yval[ii] = jupiter[ii][0][1];
  }
  // Write x and y data
  fwrite(xval, sizeof(double), nt, outFile);
  fwrite(yval, sizeof(double), nt, outFile);
  fclose(outFile);
  delete[] xval;
  delete[] yval;
  return;
}

void allocateArrays() {
  // Jupiter
  jupiter = new double** [nt];
  for (int jj = 0; jj < nt; ++jj) {
    jupiter[jj] = new double* [nVar];
    for (int ii = 0; ii < nVar; ++ii) jupiter[jj][ii] = new double[ns];
  }
  // Io
  moonIo = new double** [nt];
  for (int jj = 0; jj < nt; ++jj) {
    moonIo[jj] = new double* [nVar];
    for (int ii = 0; ii < nVar; ++ii) moonIo[jj][ii] = new double[ns];
  }
  // Calisto
  moonCal = new double** [nt];
  for (int jj = 0; jj < nt; ++jj) {
    moonCal[jj] = new double* [nVar];
    for (int ii = 0; ii < nVar; ++ii) moonCal[jj][ii] = new double[ns];
  }
  // Ganymede
  moonGan = new double** [nt];
  for (int jj = 0; jj < nt; ++jj) {
    moonGan[jj] = new double* [nVar];
    for (int ii = 0; ii < nVar; ++ii) moonGan[jj][ii] = new double[ns];
  }
  // Europa
  moonEu = new double** [nt];
  for (int jj = 0; jj < nt; ++jj) {
    moonEu[jj] = new double* [nVar];
    for (int ii = 0; ii < nVar; ++ii) moonEu[jj][ii] = new double[ns];
  }
  // Sat
  sat = new double** [nt];
  for (int jj = 0; jj < nt; ++jj) {
    sat[jj] = new double* [nVar];
    for (int ii = 0; ii < nVar; ++ii) sat[jj][ii] = new double[ns];
  }
}

void printInitialData(long int ii) {
  printf(
      "satellite:   x = %lg \t   y = %lg\n"
      "            dx = %lg \t  dy = %lg\n"
      "           ddx = %lg \t ddy = %lg\n",
      sat[ii][0][0], sat[ii][0][1], sat[ii][1][0], sat[ii][1][1], sat[ii][2][0],
      sat[ii][2][1]);
}

void readData(const char* inName) {
  static const char* finName = inName;
  double Iox(0.0), Eux(0.0), Calx(0.0), Ganx(0.0);
  double IoSign(0.0), EuSign(0.0), CalSign(0.0), GanSign(0.0);
  std::string line;
  std::string number;
  std::ifstream inDataFile(finName);
  if (inDataFile.is_open()) {
    while (inDataFile.good()) {
      // Get the current
      getline(inDataFile, line);
      // Get the numerical part of the line
      number = line.substr(line.rfind("=") + 1);
      line.erase(line.rfind("=") + 1);
      // Now we check what parameter we are receiving from
      // each line
      // and store it in the appropriate variable.
      if (strcmp(line.c_str(), "mProbe=") == 0)
        mProbe = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "mFuel=") == 0)
        mFuel = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "speed=") == 0)
        speed = atoi(number.c_str());
      else if (strcmp(line.c_str(), "V_e=") == 0)
        V_e = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "dot{m}=") == 0)
        dm = double(atof(number.c_str()));
      // Time step info
      else if (strcmp(line.c_str(), "dt=") == 0)
        dt = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "nt=") == 0)
        nt = atoi(number.c_str());
    }
  } else {
    printf("You screwed up!\n");
  }
  inDataFile.close();
  checkAnim();
  allocateArrays();
  // Set initial conditions
  initialConditions();
  std::ifstream inFile(inName);
  if (inFile.is_open()) {
    while (inFile.good()) {
      // Get the current
      getline(inFile, line);
      // Get the numerical part of the line
      number = line.substr(line.rfind("=") + 1);
      line.erase(line.rfind("=") + 1);
      if (strcmp(line.c_str(), "x=") == 0)
        sat[0][0][0] = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "y=") == 0)
        sat[0][0][1] = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "vx=") == 0)
        sat[0][1][0] = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "vy=") == 0)
        sat[0][1][1] = double(atof(number.c_str()));
      // Read in locations of the moons
      else if (strcmp(line.c_str(), "Iox=") == 0)
        Iox = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "IoSign=") == 0)
        IoSign = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "Calx=") == 0)
        Calx = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "CalSign=") == 0)
        CalSign = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "Ganx=") == 0)
        Ganx = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "GanSign=") == 0)
        GanSign = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "Eux=") == 0)
        Eux = double(atof(number.c_str()));
      else if (strcmp(line.c_str(), "EuSign=") == 0)
        EuSign = double(atof(number.c_str()));
      // Where to start burning fuel
      else if (strcmp(line.c_str(), "fuelStarty=") == 0)
        fuelStarty = double(atof(number.c_str()));
    }
  }
  inFile.close();
  mSat = mProbe + mFuel;  // Total mass
  double radius = sqrt(pow(jupiter[0][0][0] - sat[0][0][0], 2) +
                       pow(jupiter[0][0][1] - sat[0][0][1], 2));
  printf("Initial Radius of Probe is:                   %lg\n", radius);
  printf("Total Initial Mass of Probe is:               %lg\n", mSat);
  radius = 1.0 / pow(pow(jupiter[0][0][0] - sat[0][0][0], 2) +
                         pow(jupiter[0][0][1] - sat[0][0][1], 2),
                     1.5);
  // Calculate initial acceleration
  sat[0][2][0] = G * mJ * radius * (jupiter[0][0][0] - sat[0][0][0]);
  sat[0][2][1] = G * mJ * radius * (jupiter[0][0][1] - sat[0][0][1]);
  printf("The Initial Acceleration of the Satellite is: (%lg,%lg)\n",
         sat[0][2][0], sat[0][2][1]);
  printf("The Initial Coordinates are: (%lg,%lg)\n", sat[0][0][0],
         sat[0][0][1]);
  // Set position and speed of moons based off of quadrant values
  // and x coordinate
  if (fabs(Iox) <= rJtoIo) {
    if (IoSign > 1.0) IoSign = 1.0;
    if (IoSign < -1.0) IoSign = -1.0;
    moonIo[0][0][0] = Iox;
    moonIo[0][0][1] = IoSign * sqrt(pow(rJtoIo, 2) - pow(moonIo[0][0][0], 2));
    moonIo[0][1][0] = -1.0 * moonIo[0][0][1] / rJtoIo * Iov;
    moonIo[0][1][1] = moonIo[0][0][0] * Iov / rJtoIo;
  } else {
    printf(
        "Error: x-location of Io is greater than radius from "
        "Io to Jupiter.\n"
        "       Using defaults.\n");
  }
  if (fabs(Calx) <= rJtoCal) {
    if (CalSign > 1.0) CalSign = 1.0;
    if (CalSign < -1.0) CalSign = -1.0;
    moonCal[0][0][0] = Calx;
    moonCal[0][0][1] =
        CalSign * sqrt(pow(rJtoCal, 2) - pow(moonCal[0][0][0], 2));
    moonCal[0][1][0] = -1.0 * moonCal[0][0][1] / rJtoCal * Calv;
    moonCal[0][1][1] = moonCal[0][0][0] * Calv / rJtoCal;
  } else {
    printf(
        "Error: x-location of Callisto is greater than radius from "
        "Callisto to Jupiter.\n"
        "       Using defaults.\n");
  }
  if (fabs(Ganx) <= rJtoGan) {
    if (GanSign > 1.0) GanSign = 1.0;
    if (GanSign < -1.0) GanSign = -1.0;
    moonGan[0][0][0] = Ganx;
    moonGan[0][0][1] =
        GanSign * sqrt(pow(rJtoGan, 2) - pow(moonGan[0][0][0], 2));
    moonGan[0][1][0] = -1.0 * moonGan[0][0][1] / rJtoGan * Ganv;
    moonGan[0][1][1] = moonGan[0][0][0] * Ganv / rJtoGan;
  } else {
    printf(
        "Error: x-location of Ganymede is greater than radius from "
        "Ganymede to Jupiter.\n"
        "       Using defaults.\n");
  }
  if (fabs(Eux) <= rJtoEu) {
    if (EuSign > 1.0) EuSign = 1.0;
    if (EuSign < -1.0) EuSign = -1.0;
    moonEu[0][0][0] = Eux;
    moonEu[0][0][1] = EuSign * sqrt(pow(rJtoEu, 2) - pow(moonEu[0][0][0], 2));
    moonEu[0][1][0] = -1.0 * moonEu[0][0][1] / rJtoEu * Euv;
    moonEu[0][1][1] = moonEu[0][0][0] * Euv / rJtoEu;
  } else {
    printf(
        "Error: x-location of Europa is greater than radius from "
        "Europa to Jupiter.\n"
        "       Using defaults.\n");
  }
  return;
}
