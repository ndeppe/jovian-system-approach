// Include the OpenGL Files
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifndef solver_HEADER
#define solver_HEADER

// Global variables as externals
static const double PI = 3.1415926535897932384626433832;
extern double G;

extern double rJ;
extern double rIo;
extern double rCal;
extern double rEu;
extern double rGan;

extern double mJ;
extern double mIo;
extern double mCal;
extern double mEu;
extern double mGan;

extern double rJtoGan;
extern double rJtoCal;
extern double rJtoIo;
extern double rJtoEu;

extern double Iov;
extern double Ganv;
extern double Calv;
extern double Euv;

extern double mEur;
extern double mProbe;
extern double mFuel;
extern double mSat;
extern double V_e;
extern double dm;
extern double fuelStarty;
extern double dt;

extern long int nt;
extern long int ns;
extern long int nVar;

extern double*** jupiter;
extern double*** moonIo;
extern double*** moonCal;
extern double*** moonGan;
extern double*** moonEu;
extern double*** sat;

// Function definitions
// IO Funcs
void writeDataText();
void writeDataBin();
void printInitialData(long int ii);
void readData(const char* inName);
void checkAnim();

// Program flow
void initialConditions();
void tstep(long int np1t);
void allocateArrays();
long int checkCrash(long int ii);

// RHS funcs
double fSatv(double dmt, double mSt, double dJP, double rJP32, double dPG,
             double rPG32, double dPIo, double rPIo32, double dPCal,
             double rPCal32, double Ve);
double fJv(double mSt, double dJP, double rJP32, double dJG, double rJG32,
           double dJIo, double rJIo32, double dJCal, double rJCal32);
double fIov(double mSt, double dJIo, double rJIo32, double dPIo, double rPIo32,
            double dIoCal, double rIoCal32, double dIoG, double rIoG32);
double fCalv(double mSt, double dJCal, double rJCal32, double dPCal,
             double rPCal32, double dIoCal, double rIoCal32, double dCalG,
             double rCalG32);
double fEuv(double mSt, double dJEu, double rJEu32, double dPEu, double rPEu32,
            double dIoEu, double rIoEu32, double dCalEu, double rCalEu32,
            double dEuG, double rEuG32);
double fGanv(double mSt, double dJG, double rJG32, double dPG, double rPG32,
             double dIoG, double rIoG32, double dCalG, double rCalG32);

// Used in animation
double fSatv(double dmt, double mSt, double dJP, double rJP32, double dPG,
             double rPG32, double dPIo, double rPIo32, double dPCal,
             double rPCal32, double dPEu, double rPEu32, double Ve);
double fJv(double mSt, double dJP, double rJP32, double dJG, double rJG32,
           double dJIo, double rJIo32, double dJEu, double rJEu32, double dJCal,
           double rJCal32);
double fCalv(double mSt, double dJCal, double rJCal32, double dPCal,
             double rPCal32, double dIoCal, double rIoCal32, double dCalEu,
             double rCalEu32, double dCalG, double rCalG32);
double fIov(double mSt, double dJIo, double rJIo32, double dPIo, double rPIo32,
            double dIoCal, double rIoCal32, double dIoEu, double rIoEu32,
            double dIoG, double rIoG32);
double fGanv(double mSt, double dJG, double rJG32, double dPG, double rPG32,
             double dIoG, double rIoG32, double dCalG, double rCalG32,
             double dEuG, double rEuG32);

// OpenGL Functions
void handleKeypress(unsigned char key, int x, int y);
void print_bitmap_string(void* font, const char* s);
int bitmapSize(void* font, const char* s);
void initRendering();
void handleResize(int w, int h);
void drawScene();
void update(int value);
void printExtremums();
void drawGradientBox();
void drawTimeBackground();
void animateTime();
void printTime();
void drawFuel();
void drawCrashSite();
void printTime();

extern GLenum errCode;
extern const GLubyte* errString;
extern long int cTime;
extern double shipSize;
extern int speed;

#endif // 2Dsolver_HEADER
