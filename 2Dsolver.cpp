#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <math.h>
// Include our header file
#include "2Dsolver.hpp"

// Physical constants
double G = 6.67428e-20;  // in terms of km
// Mass of the different moons and Jupiter
double mJ = 1898.3e24;   // Jupiter mass in kg
double mIo = 89.3e021;   // Io mass in kg 21
double mCal = 107.6e21;  // Calisto in kg 21
double mGan = 148.2e21;  // Ganymede 21
double mEur = 48.0e21;   // Europa
// Radii of the different moons and Jupiter
double rJ = 71492;   // Jupiter radius
double rGan = 2631;  // Radius of Ganymede
double rCal = 2410;  // Radius of Callisto
double rIo = 1821;   // Radius of Io
double rEu = 1561;   // Radius of Europa
// Separation radii from Jupiter to the moons
double rJtoGan = 1.070e6;  // Radius from Jupiter to Ganymede
double rJtoCal = 1.883e6;  // Radius from Jupiter to Callisto
double rJtoIo = 4.22e5;    // Radius from Jupiter to Io
double rJtoEu = 6.71e5;    // Radius from Jupiter to Europa
// ND The tangential velocity of the moons
double Ganv = 10.9;  // Ganymede tangential velocity
double Iov = 17.3;
double Calv = 8.2;
double Euv = 13.7;
// Satellite variables
double mFuel = 77.0;
double mProbe = 401.0;
double mSat = mFuel + mProbe;
double V_e = 2.27;
double dm = -17.6e-4;
double fuelStarty = 1.0e10;
// Time and location when fuel ran out
double fuelTime = -100;
double fuelLoc[2] = {0.0, 0.0};
int ranOut = 0;
// Constant in RK4 method
static const double a0 = 1.0 / 6.0;

// Used only in animation
int speed = 10;
long int cTime = 0;

// Spatial and temporal mesh spacing
double dt = 10.0;
// Number of time steps to take
long int nt = 14000;
// number of spatial coordinates, use x, y
long int ns = 2;
// Number of "variables" per
long int nVar = 3;
// Number of objects in our system
long int nObj = 2;

// Make global arrays for the different bodies
// indices mean [t][obj][derivative order][0=x,1=y]
// deriv [0][0] = x, deriv [1][0] = v^x, deriv [2][0] = \dot{v}^x
//                x               \dot{x}               \ddot{x}
double*** jupiter;
double*** moonIo;
double*** moonCal;
double*** moonGan;
double*** moonEu;
double*** sat;

int main(int argc, char* argv[]) {
  long int crash = 0;  // if 1, we crashed...
  if (argc != 2) {
    printf(
        "Wrong number of arguments! "
        "Specify the input file as follows please.\n"
        "./probeData input.txt\n");
    return -1;
  }
  readData(argv[1]);
  // Take nt time steps
  for (long int ii = 1; ii < nt; ++ii) {
    tstep(ii);
    crash = checkCrash(ii);
    if (crash == 1) break;
  }
  // Write out ALL data from all time steps. Mainly used for debugging.
  // writeDataText();
  // Write data for plotting in ROOT
  writeDataBin();
  printf("Final orbital radius of satellite: %lg.\n",
         sqrt(pow(sat[nt - 1][0][0], 2) + pow(sat[nt - 1][0][1], 2)));
  printf("Run time: %lg.\n", nt * dt);
  return 0;
}

void initialConditions() {
  // Initial Jupiter conditions
  jupiter[0][0][0] = 0.0;
  jupiter[0][0][1] = 0.0;
  jupiter[0][1][0] = 0.0;
  jupiter[0][1][1] = 0.0;
  jupiter[0][2][0] = 0.0;
  jupiter[0][2][1] = 0.0;
  // Initial Io moon conditions
  moonIo[0][0][0] = rJtoIo;
  moonIo[0][0][1] = sqrt(pow(rJtoIo, 2) - pow(moonIo[0][0][0], 2));
  moonIo[0][1][0] = 0.0;
  moonIo[0][1][1] = Iov;
  moonIo[0][2][0] = -pow(moonIo[0][1][1], 2) / moonIo[0][0][0];
  moonIo[0][2][1] = 0.0;
  // Initial Callisto  moon conditions
  moonCal[0][0][0] = rJtoCal;
  moonCal[0][0][1] = sqrt(pow(rJtoCal, 2) - pow(moonCal[0][0][0], 2));
  moonCal[0][1][0] = 0.0;
  moonCal[0][1][1] = Calv;
  moonCal[0][2][0] = -pow(moonCal[0][1][1], 2) / moonCal[0][0][0];
  moonCal[0][2][1] = 0.0;
  // Initial Ganymede  moon conditions
  moonGan[0][0][0] = rJtoGan;
  moonGan[0][0][1] = 0.0;  // sqrt(pow(radius,2)-pow(moonGan[0][0][0],2));
  moonGan[0][1][0] = 0.0;
  moonGan[0][1][1] = Ganv;
  moonGan[0][2][0] = -pow(moonGan[0][1][1], 2) / moonGan[0][0][0];
  moonGan[0][2][1] = 0.0;
  // Initial Europa  moon conditions
  moonEu[0][0][0] = rJtoEu;
  moonEu[0][0][1] = 0.0;
  moonEu[0][1][0] = 0.0;
  moonEu[0][1][1] = Euv;
  moonEu[0][2][0] = -pow(moonEu[0][1][1], 2) / moonEu[0][0][0];
  moonEu[0][2][1] = 0.0;
}

// This when called takes one step forward in
// coordinate time
void tstep(long int np1t) {
  ////////////////////////////////////////////////////////////////
  // PROBE
  // Mass of probe
  double dmt = dm;
  // Velocity of probe
  double vpNorm =
      sqrt(pow(sat[np1t - 1][1][0], 2) + pow(sat[np1t - 1][1][1], 2));
  double Ve[2] = {V_e * sat[np1t - 1][1][0] / vpNorm,
                  V_e * sat[np1t - 1][1][1] / vpNorm};
  // Temp vectors for Jupiter and Probe, denominator and numerator
  double rJP32 =
      1.0 / pow(pow(sat[np1t - 1][0][0] - jupiter[np1t - 1][0][0], 2) +
                    pow(sat[np1t - 1][0][1] - jupiter[np1t - 1][0][1], 2),
                1.5);
  double dJP = 1.0e200;  // distance in ii direction (x or y)
  // Temp vectors for Probe and Ganymede, denominator and numerator
  double rPG32 = 1.0e200;
  rPG32 = 1.0 / pow(pow(sat[np1t - 1][0][0] - moonGan[np1t - 1][0][0], 2) +
                        pow(sat[np1t - 1][0][1] - moonGan[np1t - 1][0][1], 2),
                    1.5);
  double dPG = 1.0e200;
  // Temp vectors for Probe and Io, denominator and numerator
  double rPIo32 =
      1.0 / pow(pow(sat[np1t - 1][0][0] - moonIo[np1t - 1][0][0], 2) +
                    pow(sat[np1t - 1][0][1] - moonIo[np1t - 1][0][1], 2),
                1.5);
  double dPIo = 1.0e200;
  // Temp vectors for Probe and Callisto, denominator and numerator
  double rPCal32 =
      1.0 / pow(pow(sat[np1t - 1][0][0] - moonCal[np1t - 1][0][0], 2) +
                    pow(sat[np1t - 1][0][1] - moonCal[np1t - 1][0][1], 2),
                1.5);
  double dPCal = 1.0e200;
  // Temp vectors for Probe and Europa, denominator and numerator
  double rPEu32 =
      1.0 / pow(pow(sat[np1t - 1][0][0] - moonEu[np1t - 1][0][0], 2) +
                    pow(sat[np1t - 1][0][1] - moonEu[np1t - 1][0][1], 2),
                1.5);
  double dPEu = 1.0e200;
  ////////////////////////////////////////////////////////////
  // JUPITER
  // Temp vectors for Jupiter and Ganymede, denominator and numerator
  double rJG32 =
      1.0 / pow(pow(moonGan[np1t - 1][0][0] - jupiter[np1t - 1][0][0], 2) +
                    pow(moonGan[np1t - 1][0][1] - jupiter[np1t - 1][0][1], 2),
                1.5);
  double dJG = 1.0e200;  // distance between Jup and Gan
  // Temp vectors for Jupiter and Io, denominator and numerator
  double rJIo32 =
      1.0 / pow(pow(moonIo[np1t - 1][0][0] - jupiter[np1t - 1][0][0], 2) +
                    pow(moonIo[np1t - 1][0][1] - jupiter[np1t - 1][0][1], 2),
                1.5);
  double dJIo = 1.0e200;  // distance between Jup and Io
  // Temp vectors for Jupiter and Callisto, denominator and numerator
  double rJCal32 =
      1.0 / pow(pow(moonCal[np1t - 1][0][0] - jupiter[np1t - 1][0][0], 2) +
                    pow(moonCal[np1t - 1][0][1] - jupiter[np1t - 1][0][1], 2),
                1.5);
  double dJCal = 1.0e200;  // distance between Jup and Callisto
  // Temp vectors for Jupiter and Europa, denominator and numerator
  double rJEu32 =
      1.0 / pow(pow(moonEu[np1t - 1][0][0] - jupiter[np1t - 1][0][0], 2) +
                    pow(moonEu[np1t - 1][0][1] - jupiter[np1t - 1][0][1], 2),
                1.5);
  double dJEu = 1.0e200;  // distance between Jup and Callisto
  ////////////////////////////////////////////////////////////
  // Io
  // Temp vectors for Ganymede and Io, denominator and numerator
  double rIoG32 =
      1.0 / pow(pow(moonGan[np1t - 1][0][0] - moonIo[np1t - 1][0][0], 2) +
                    pow(moonGan[np1t - 1][0][1] - moonIo[np1t - 1][0][1], 2),
                1.5);
  double dIoG = 1.0e200;  // distance between Gan and Io
  // Temp vectors for Ganymede and Io, denominator and numerator
  double rIoCal32 =
      1.0 / pow(pow(moonCal[np1t - 1][0][0] - moonIo[np1t - 1][0][0], 2) +
                    pow(moonCal[np1t - 1][0][1] - moonIo[np1t - 1][0][1], 2),
                1.5);
  double dIoCal = 1.0e200;  // distance between Gan and Io
  // Temp vectors for Europa and Io, denominator and numerator
  double rIoEu32 =
      1.0 / pow(pow(moonEu[np1t - 1][0][0] - moonIo[np1t - 1][0][0], 2) +
                    pow(moonEu[np1t - 1][0][1] - moonIo[np1t - 1][0][1], 2),
                1.5);
  double dIoEu = 1.0e200;  // distance between Gan and Io
  ////////////////////////////////////////////////////////////
  // Callisto
  // Temp vectors for Ganymede and Callisto, denominator and numerator
  double rCalG32 =
      1.0 / pow(pow(moonGan[np1t - 1][0][0] - moonCal[np1t - 1][0][0], 2) +
                    pow(moonGan[np1t - 1][0][1] - moonCal[np1t - 1][0][1], 2),
                1.5);
  double dCalG = 1.0e200;
  // Temp vectors for Ganymede and Callisto, denominator and numerator
  double rCalEu32 =
      1.0 / pow(pow(moonGan[np1t - 1][0][0] - moonEu[np1t - 1][0][0], 2) +
                    pow(moonGan[np1t - 1][0][1] - moonEu[np1t - 1][0][1], 2),
                1.5);
  double dCalEu = 1.0e200;
  ////////////////////////////////////////////////////////////
  // Europa
  // Temp vectors for Ganymede and Callisto, denominator and numerator
  double rEuG32 =
      1.0 / pow(pow(moonGan[np1t - 1][0][0] - moonEu[np1t - 1][0][0], 2) +
                    pow(moonGan[np1t - 1][0][1] - moonEu[np1t - 1][0][1], 2),
                1.5);
  double dEuG = 1.0e200;
  if (mSat <= mProbe && ranOut == 0) {
    printf("Time: %lg s, coords: (%lg,%lg)\n", (np1t - 1) * dt,
           sat[np1t - 1][0][0], sat[np1t - 1][0][1]);
    ranOut = 1;
  }
  if (sat[np1t - 1][0][1] > fuelStarty)
    dmt = dm;
  else
    dmt = 0.0;
  if (mSat <= mProbe) {
    dmt = 0.0;
    mSat = mProbe;
  }
  // Number of variables
  double k1Jv[2], k1Jr[2], k1Sv[2], k1Sr[2], k1mGv[2], k1mGr[2];
  double k2Jv[2], k2Jr[2], k2Sv[2], k2Sr[2], k2mGv[2], k2mGr[2];
  double k3Jv[2], k3Jr[2], k3Sv[2], k3Sr[2], k3mGv[2], k3mGr[2];
  double k4Jv[2], k4Jr[2], k4Sv[2], k4Sr[2], k4mGv[2], k4mGr[2];
  double k1mIov[2], k1mIor[2], k1mCalr[2], k1mCalv[2];
  double k2mIov[2], k2mIor[2], k2mCalr[2], k2mCalv[2];
  double k3mIov[2], k3mIor[2], k3mCalr[2], k3mCalv[2];
  double k4mIov[2], k4mIor[2], k4mCalr[2], k4mCalv[2];
  double k1mEuv[2], k1mEur[2];
  double k2mEuv[2], k2mEur[2];
  double k3mEuv[2], k3mEur[2];
  double k4mEuv[2], k4mEur[2];
  // Do first RK step
  for (long int ii = 0; ii < ns; ++ii) {
    dJP = sat[np1t - 1][0][ii] - jupiter[np1t - 1][0][ii];  // r_JP=-r_PJ
    dPG = moonGan[np1t - 1][0][ii] - sat[np1t - 1][0][ii];
    dPIo = moonIo[np1t - 1][0][ii] - sat[np1t - 1][0][ii];
    dPCal = moonCal[np1t - 1][0][ii] - sat[np1t - 1][0][ii];
    dPEu = moonEu[np1t - 1][0][ii] - sat[np1t - 1][0][ii];

    dJG = moonGan[np1t - 1][0][ii] - jupiter[np1t - 1][0][ii];
    dJIo = moonIo[np1t - 1][0][ii] - jupiter[np1t - 1][0][ii];
    dJCal = moonCal[np1t - 1][0][ii] - jupiter[np1t - 1][0][ii];
    dJEu = moonEu[np1t - 1][0][ii] - jupiter[np1t - 1][0][ii];

    dIoG = moonGan[np1t - 1][0][ii] - moonIo[np1t - 1][0][ii];
    dIoCal = moonCal[np1t - 1][0][ii] - moonIo[np1t - 1][0][ii];
    dIoEu = moonEu[np1t - 1][0][ii] - moonIo[np1t - 1][0][ii];

    dCalG = moonGan[np1t - 1][0][ii] - moonCal[np1t - 1][0][ii];
    dCalEu = moonEu[np1t - 1][0][ii] - moonCal[np1t - 1][0][ii];

    dEuG = moonGan[np1t - 1][0][ii] - moonEu[np1t - 1][0][ii];

    k1Sv[ii] = dt * fSatv(dmt, mSat, dJP, rJP32, dPG, rPG32, dPIo, rPIo32,
                          dPCal, rPCal32, dPEu, rPEu32, Ve[ii]);
    k1Sr[ii] = dt * sat[np1t - 1][1][ii];
    k1Jv[ii] = dt * fJv(mSat, dJP, rJP32, dJG, rJG32, dJIo, rJIo32, dJEu,
                        rJEu32, dJCal, rJCal32);
    k1Jr[ii] = dt * jupiter[np1t - 1][1][ii];
    k1mIov[ii] = dt * fIov(mSat, dJIo, rJIo32, dPIo, rPIo32, dIoCal, rIoCal32,
                           dIoEu, rIoEu32, dIoG, rIoG32);
    k1mIor[ii] = dt * moonIo[np1t - 1][1][ii];
    k1mCalv[ii] = dt * fCalv(mSat, dJCal, rJCal32, dPCal, rPCal32, dIoCal,
                             rIoCal32, dCalEu, rCalEu32, dCalG, rCalG32);
    k1mCalr[ii] = dt * moonCal[np1t - 1][1][ii];
    k1mEuv[ii] = dt * fEuv(mSat, dJEu, rJEu32, dPEu, rPEu32, dIoEu, rIoEu32,
                           dCalEu, rCalEu32, dEuG, rEuG32);
    k1mEur[ii] = dt * moonEu[np1t - 1][1][ii];
    k1mGv[ii] = dt * fGanv(mSat, dJG, rJG32, dPG, rPG32, dIoG, rIoG32, dCalG,
                           rCalG32, dEuG, rEuG32);
    k1mGr[ii] = dt * moonGan[np1t - 1][1][ii];
  }
  // Set in between values for the 2nd step
  ////////////////////////////////////////////////////////////
  // PROBE
  rJP32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k1Sr[0] -
                            jupiter[np1t - 1][0][0] - 0.5 * k1Jr[0],
                        2) +
                        pow(sat[np1t - 1][0][1] + 0.5 * k1Sr[1] -
                                jupiter[np1t - 1][0][1] - 0.5 * k1Jr[1],
                            2),
                    1.5);
  rPG32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k1Sr[0] -
                            moonGan[np1t - 1][0][0] - 0.5 * k1mGr[0],
                        2) +
                        pow(sat[np1t - 1][0][1] + 0.5 * k1Sr[1] -
                                moonGan[np1t - 1][0][1] - 0.5 * k1mGr[1],
                            2),
                    1.5);
  rPIo32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k1Sr[0] -
                             moonIo[np1t - 1][0][0] - 0.5 * k1mIor[0],
                         2) +
                         pow(sat[np1t - 1][0][1] + 0.5 * k1Sr[1] -
                                 moonIo[np1t - 1][0][1] - 0.5 * k1mIor[1],
                             2),
                     1.5);
  rPCal32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k1Sr[0] -
                              moonCal[np1t - 1][0][0] - 0.5 * k1mCalr[0],
                          2) +
                          pow(sat[np1t - 1][0][1] + 0.5 * k1Sr[1] -
                                  moonCal[np1t - 1][0][1] - 0.5 * k1mCalr[1],
                              2),
                      1.5);
  rPEu32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k1Sr[0] -
                             moonEu[np1t - 1][0][0] - 0.5 * k1mEur[0],
                         2) +
                         pow(sat[np1t - 1][0][1] + 0.5 * k1Sr[1] -
                                 moonEu[np1t - 1][0][1] - 0.5 * k1mEur[1],
                             2),
                     1.5);
  ////////////////////////////////////////////////////////////
  // JUPITER
  rJG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k1mGr[0] -
                            jupiter[np1t - 1][0][0] - 0.5 * k1Jr[0],
                        2) +
                        pow(moonGan[np1t - 1][0][1] + 0.5 * k1mGr[1] -
                                jupiter[np1t - 1][0][1] - 0.5 * k1Jr[1],
                            2),
                    1.5);
  rJIo32 = 1.0 / pow(pow(moonIo[np1t - 1][0][0] + 0.5 * k1mIor[0] -
                             jupiter[np1t - 1][0][0] - 0.5 * k1Jr[0],
                         2) +
                         pow(moonIo[np1t - 1][0][1] + 0.5 * k1mIor[1] -
                                 jupiter[np1t - 1][0][1] - 0.5 * k1Jr[1],
                             2),
                     1.5);
  rJCal32 = 1.0 / pow(pow(moonCal[np1t - 1][0][0] + 0.5 * k1mCalr[0] -
                              jupiter[np1t - 1][0][0] - 0.5 * k1Jr[0],
                          2) +
                          pow(moonCal[np1t - 1][0][1] + 0.5 * k1mCalr[1] -
                                  jupiter[np1t - 1][0][1] - 0.5 * k1Jr[1],
                              2),
                      1.5);
  rJEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k1mEur[0] -
                             jupiter[np1t - 1][0][0] - 0.5 * k1Jr[0],
                         2) +
                         pow(moonEu[np1t - 1][0][1] + 0.5 * k1mEur[1] -
                                 jupiter[np1t - 1][0][1] - 0.5 * k1Jr[1],
                             2),
                     1.5);
  ////////////////////////////////////////////////////////////
  // Io
  rIoG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k1mGr[0] -
                             moonIo[np1t - 1][0][0] - 0.5 * k1mIor[0],
                         2) +
                         pow(moonGan[np1t - 1][0][1] + 0.5 * k1mGr[1] -
                                 moonIo[np1t - 1][0][1] - 0.5 * k1mIor[1],
                             2),
                     1.5);
  rIoCal32 = 1.0 / pow(pow(moonCal[np1t - 1][0][0] + 0.5 * k1mCalr[0] -
                               moonIo[np1t - 1][0][0] - 0.5 * k1mIor[0],
                           2) +
                           pow(moonCal[np1t - 1][0][1] + 0.5 * k1mCalr[1] -
                                   moonIo[np1t - 1][0][1] - 0.5 * k1mIor[1],
                               2),
                       1.5);
  rIoEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k1mEur[0] -
                              moonIo[np1t - 1][0][0] - 0.5 * k1mIor[0],
                          2) +
                          pow(moonEu[np1t - 1][0][1] + 0.5 * k1mEur[1] -
                                  moonIo[np1t - 1][0][1] - 0.5 * k1mIor[1],
                              2),
                      1.5);
  ////////////////////////////////////////////////////////////
  // Callisto
  rCalG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k1mGr[0] -
                              moonCal[np1t - 1][0][0] - 0.5 * k1mCalr[0],
                          2) +
                          pow(moonGan[np1t - 1][0][1] + 0.5 * k1mGr[1] -
                                  moonCal[np1t - 1][0][1] - 0.5 * k1mCalr[1],
                              2),
                      1.5);
  rCalEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k1mEur[0] -
                               moonCal[np1t - 1][0][0] - 0.5 * k1mCalr[0],
                           2) +
                           pow(moonEu[np1t - 1][0][1] + 0.5 * k1mEur[1] -
                                   moonCal[np1t - 1][0][1] - 0.5 * k1mCalr[1],
                               2),
                       1.5);
  ////////////////////////////////////////////////////////////
  // Europa
  rEuG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k1mGr[0] -
                             moonEu[np1t - 1][0][0] - 0.5 * k1mEur[0],
                         2) +
                         pow(moonGan[np1t - 1][0][1] + 0.5 * k1mGr[1] -
                                 moonEu[np1t - 1][0][1] - 0.5 * k1mEur[1],
                             2),
                     1.5);

  vpNorm = sqrt(pow(sat[np1t - 1][1][0] + 0.5 * k1Sv[0], 2) +
                pow(sat[np1t - 1][1][1] + 0.5 * k1Sv[1], 2));
  Ve[0] = V_e * (sat[np1t - 1][1][0] + 0.5 * k1Sv[0]) / vpNorm;
  Ve[1] = V_e * (sat[np1t - 1][1][1] + 0.5 * k1Sv[1]) / vpNorm;
  if (mSat > mProbe) mSat += 0.5 * dt * dm;  // dm < 0
  // Do second RK step
  for (long int ii = 0; ii < ns; ++ii) {
    dJP = sat[np1t - 1][0][ii] + 0.5 * k1Sr[ii] - jupiter[np1t - 1][0][ii] -
          0.5 * k1Jr[ii];
    dPG = moonGan[np1t - 1][0][ii] + 0.5 * k1mGr[ii] - sat[np1t - 1][0][ii] -
          0.5 * k1Sr[ii];
    dPIo = moonIo[np1t - 1][0][ii] + 0.5 * k1mIor[ii] - sat[np1t - 1][0][ii] -
           0.5 * k1Sr[ii];
    dPCal = moonCal[np1t - 1][0][ii] + 0.5 * k1mCalr[ii] -
            sat[np1t - 1][0][ii] - 0.5 * k1Sr[ii];
    dPEu = moonEu[np1t - 1][0][ii] + 0.5 * k1mEur[ii] - sat[np1t - 1][0][ii] -
           0.5 * k1Sr[ii];

    dJG = moonGan[np1t - 1][0][ii] + 0.5 * k1mGr[ii] -
          jupiter[np1t - 1][0][ii] - 0.5 * k1Jr[ii];
    dJIo = moonIo[np1t - 1][0][ii] + 0.5 * k1mIor[ii] -
           jupiter[np1t - 1][0][ii] - 0.5 * k1Jr[ii];
    dJCal = moonCal[np1t - 1][0][ii] + 0.5 * k1mCalr[ii] -
            jupiter[np1t - 1][0][ii] - 0.5 * k1Jr[ii];
    dJEu = moonEu[np1t - 1][0][ii] + 0.5 * k1mEur[ii] -
           jupiter[np1t - 1][0][ii] - 0.5 * k1Jr[ii];

    dIoG = moonGan[np1t - 1][0][ii] + 0.5 * k1mGr[ii] -
           moonIo[np1t - 1][0][ii] - 0.5 * k1mIor[ii];
    dIoCal = moonCal[np1t - 1][0][ii] + 0.5 * k1mCalr[ii] -
             moonIo[np1t - 1][0][ii] - 0.5 * k1mIor[ii];
    dIoEu = moonEu[np1t - 1][0][ii] + 0.5 * k1mEur[ii] -
            moonIo[np1t - 1][0][ii] - 0.5 * k1mIor[ii];

    dCalG = moonGan[np1t - 1][0][ii] + 0.5 * k1mGr[ii] -
            moonCal[np1t - 1][0][ii] - 0.5 * k1mCalr[ii];
    dCalEu = moonEu[np1t - 1][0][ii] + 0.5 * k1mEur[ii] -
             moonCal[np1t - 1][0][ii] - 0.5 * k1mCalr[ii];

    dEuG = moonGan[np1t - 1][0][ii] + 0.5 * k1mGr[ii] -
           moonEu[np1t - 1][0][ii] - 0.5 * k1mEur[ii];

    k2Sv[ii] = dt * fSatv(dmt, mSat, dJP, rJP32, dPG, rPG32, dPIo, rPIo32,
                          dPCal, rPCal32, dPEu, rPEu32, Ve[ii]);
    k2Sr[ii] = dt * sat[np1t - 1][1][ii];
    k2Jv[ii] = dt * fJv(mSat, dJP, rJP32, dJG, rJG32, dJIo, rJIo32, dJEu,
                        rJEu32, dJCal, rJCal32);
    k2Jr[ii] = dt * jupiter[np1t - 1][1][ii];
    k2mIov[ii] = dt * fIov(mSat, dJIo, rJIo32, dPIo, rPIo32, dIoCal, rIoCal32,
                           dIoEu, rIoEu32, dIoG, rIoG32);
    k2mIor[ii] = dt * moonIo[np1t - 1][1][ii];
    k2mCalv[ii] = dt * fCalv(mSat, dJCal, rJCal32, dPCal, rPCal32, dIoCal,
                             rIoCal32, dCalEu, rCalEu32, dCalG, rCalG32);
    k2mCalr[ii] = dt * moonCal[np1t - 1][1][ii];
    k2mEuv[ii] = dt * fEuv(mSat, dJEu, rJEu32, dPEu, rPEu32, dIoEu, rIoEu32,
                           dCalEu, rCalEu32, dEuG, rEuG32);
    k2mEur[ii] = dt * moonEu[np1t - 1][1][ii];
    k2mGv[ii] = dt * fGanv(mSat, dJG, rJG32, dPG, rPG32, dIoG, rIoG32, dCalG,
                           rCalG32, dEuG, rEuG32);
    k2mGr[ii] = dt * moonGan[np1t - 1][1][ii];
  }
  // Set in between values for the 3rd step
  ////////////////////////////////////////////////////////////
  // PROBE
  rJP32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k2Sr[0] -
                            jupiter[np1t - 1][0][0] - 0.5 * k2Jr[0],
                        2) +
                        pow(sat[np1t - 1][0][1] + 0.5 * k2Sr[1] -
                                jupiter[np1t - 1][0][1] - 0.5 * k2Jr[1],
                            2),
                    1.5);
  rPG32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k2Sr[0] -
                            moonGan[np1t - 1][0][0] - 0.5 * k2mGr[0],
                        2) +
                        pow(sat[np1t - 1][0][1] + 0.5 * k2Sr[1] -
                                moonGan[np1t - 1][0][1] - 0.5 * k2mGr[1],
                            2),
                    1.5);
  rPIo32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k2Sr[0] -
                             moonIo[np1t - 1][0][0] - 0.5 * k2mIor[0],
                         2) +
                         pow(sat[np1t - 1][0][1] + 0.5 * k2Sr[1] -
                                 moonIo[np1t - 1][0][1] - 0.5 * k2mIor[1],
                             2),
                     1.5);
  rPCal32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k2Sr[0] -
                              moonCal[np1t - 1][0][0] - 0.5 * k2mCalr[0],
                          2) +
                          pow(sat[np1t - 1][0][1] + 0.5 * k2Sr[1] -
                                  moonCal[np1t - 1][0][1] - 0.5 * k2mCalr[1],
                              2),
                      1.5);
  rPEu32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k2Sr[0] -
                             moonEu[np1t - 1][0][0] - 0.5 * k2mEur[0],
                         2) +
                         pow(sat[np1t - 1][0][1] + 0.5 * k2Sr[1] -
                                 moonEu[np1t - 1][0][1] - 0.5 * k2mEur[1],
                             2),
                     1.5);
  ////////////////////////////////////////////////////////////
  // JUPITER
  rJG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k2mGr[0] -
                            jupiter[np1t - 1][0][0] - 0.5 * k2Jr[0],
                        2) +
                        pow(moonGan[np1t - 1][0][1] + 0.5 * k2mGr[1] -
                                jupiter[np1t - 1][0][1] - 0.5 * k2Jr[1],
                            2),
                    1.5);
  rJIo32 = 1.0 / pow(pow(moonIo[np1t - 1][0][0] + 0.5 * k2mIor[0] -
                             jupiter[np1t - 1][0][0] - 0.5 * k2Jr[0],
                         2) +
                         pow(moonIo[np1t - 1][0][1] + 0.5 * k2mIor[1] -
                                 jupiter[np1t - 1][0][1] - 0.5 * k2Jr[1],
                             2),
                     1.5);
  rJCal32 = 1.0 / pow(pow(moonCal[np1t - 1][0][0] + 0.5 * k2mCalr[0] -
                              jupiter[np1t - 1][0][0] - 0.5 * k2Jr[0],
                          2) +
                          pow(moonCal[np1t - 1][0][1] + 0.5 * k2mCalr[1] -
                                  jupiter[np1t - 1][0][1] - 0.5 * k2Jr[1],
                              2),
                      1.5);
  rJEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k2mEur[0] -
                             jupiter[np1t - 1][0][0] - 0.5 * k2Jr[0],
                         2) +
                         pow(moonEu[np1t - 1][0][1] + 0.5 * k2mEur[1] -
                                 jupiter[np1t - 1][0][1] - 0.5 * k2Jr[1],
                             2),
                     1.5);
  ////////////////////////////////////////////////////////////
  // Io
  rIoG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k2mGr[0] -
                             moonIo[np1t - 1][0][0] - 0.5 * k2mIor[0],
                         2) +
                         pow(moonGan[np1t - 1][0][1] + 0.5 * k2mGr[1] -
                                 moonIo[np1t - 1][0][1] - 0.5 * k2mIor[1],
                             2),
                     1.5);
  rIoCal32 = 1.0 / pow(pow(moonCal[np1t - 1][0][0] + 0.5 * k2mCalr[0] -
                               moonIo[np1t - 1][0][0] - 0.5 * k2mIor[0],
                           2) +
                           pow(moonCal[np1t - 1][0][1] + 0.5 * k2mCalr[1] -
                                   moonIo[np1t - 1][0][1] - 0.5 * k2mIor[1],
                               2),
                       1.5);
  rIoEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k2mEur[0] -
                              moonIo[np1t - 1][0][0] - 0.5 * k2mIor[0],
                          2) +
                          pow(moonEu[np1t - 1][0][1] + 0.5 * k2mEur[1] -
                                  moonIo[np1t - 1][0][1] - 0.5 * k2mIor[1],
                              2),
                      1.5);
  ////////////////////////////////////////////////////////////
  // Cal
  rCalG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k2mGr[0] -
                              moonCal[np1t - 1][0][0] - 0.5 * k2mCalr[0],
                          2) +
                          pow(moonGan[np1t - 1][0][1] + 0.5 * k2mGr[1] -
                                  moonCal[np1t - 1][0][1] - 0.5 * k2mCalr[1],
                              2),
                      1.5);
  rCalEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k2mEur[0] -
                               moonCal[np1t - 1][0][0] - 0.5 * k2mCalr[0],
                           2) +
                           pow(moonEu[np1t - 1][0][1] + 0.5 * k2mEur[1] -
                                   moonCal[np1t - 1][0][1] - 0.5 * k2mCalr[1],
                               2),
                       1.5);
  ////////////////////////////////////////////////////////////
  // Europa
  rEuG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k2mGr[0] -
                             moonEu[np1t - 1][0][0] - 0.5 * k2mEur[0],
                         2) +
                         pow(moonGan[np1t - 1][0][1] + 0.5 * k2mGr[1] -
                                 moonEu[np1t - 1][0][1] - 0.5 * k2mEur[1],
                             2),
                     1.5);

  vpNorm = sqrt(pow(sat[np1t - 1][1][0] + 0.5 * k2Sv[0], 2) +
                pow(sat[np1t - 1][1][1] + 0.5 * k2Sv[1], 2));
  Ve[0] = V_e * (sat[np1t - 1][1][0] + 0.5 * k2Sv[0]) / vpNorm;
  Ve[1] = V_e * (sat[np1t - 1][1][1] + 0.5 * k2Sv[1]) / vpNorm;
  // Do third RK step
  for (long int ii = 0; ii < ns; ++ii) {
    dJP = sat[np1t - 1][0][ii] + 0.5 * k2Sr[ii] - jupiter[np1t - 1][0][ii] -
          0.5 * k2Jr[ii];
    dPG = moonGan[np1t - 1][0][ii] + 0.5 * k2mGr[ii] - sat[np1t - 1][0][ii] -
          0.5 * k2Sr[ii];
    dPIo = moonIo[np1t - 1][0][ii] + 0.5 * k2mIor[ii] - sat[np1t - 1][0][ii] -
           0.5 * k2Sr[ii];
    dPCal = moonCal[np1t - 1][0][ii] + 0.5 * k2mCalr[ii] -
            sat[np1t - 1][0][ii] - 0.5 * k2Sr[ii];
    dPEu = moonEu[np1t - 1][0][ii] + 0.5 * k2mEur[ii] - sat[np1t - 1][0][ii] -
           0.5 * k2Sr[ii];

    dJG = moonGan[np1t - 1][0][ii] + 0.5 * k2mGr[ii] -
          jupiter[np1t - 1][0][ii] - 0.5 * k2Jr[ii];
    dJIo = moonIo[np1t - 1][0][ii] + 0.5 * k2mIor[ii] -
           jupiter[np1t - 1][0][ii] - 0.5 * k2Jr[ii];
    dJCal = moonCal[np1t - 1][0][ii] + 0.5 * k2mCalr[ii] -
            jupiter[np1t - 1][0][ii] - 0.5 * k2Jr[ii];
    dJEu = moonEu[np1t - 1][0][ii] + 0.5 * k2mEur[ii] -
           jupiter[np1t - 1][0][ii] - 0.5 * k2Jr[ii];

    dIoG = moonGan[np1t - 1][0][ii] + 0.5 * k2mGr[ii] -
           moonIo[np1t - 1][0][ii] - 0.5 * k2mIor[ii];
    dIoCal = moonCal[np1t - 1][0][ii] + 0.5 * k2mCalr[ii] -
             moonIo[np1t - 1][0][ii] - 0.5 * k2mIor[ii];
    dIoEu = moonEu[np1t - 1][0][ii] + 0.5 * k2mEur[ii] -
            moonIo[np1t - 1][0][ii] - 0.5 * k2mIor[ii];

    dCalG = moonGan[np1t - 1][0][ii] + 0.5 * k2mGr[ii] -
            moonCal[np1t - 1][0][ii] - 0.5 * k2mCalr[ii];
    dCalEu = moonEu[np1t - 1][0][ii] + 0.5 * k2mEur[ii] -
             moonCal[np1t - 1][0][ii] - 0.5 * k2mCalr[ii];

    dEuG = moonGan[np1t - 1][0][ii] + 0.5 * k2mGr[ii] -
           moonEu[np1t - 1][0][ii] - 0.5 * k2mEur[ii];

    k3Sv[ii] = dt * fSatv(dmt, mSat, dJP, rJP32, dPG, rPG32, dPIo, rPIo32,
                          dPCal, rPCal32, dPEu, rPEu32, Ve[ii]);
    k3Sr[ii] = dt * sat[np1t - 1][1][ii];
    k3Jv[ii] = dt * fJv(mSat, dJP, rJP32, dJG, rJG32, dJIo, rJIo32, dJEu,
                        rJEu32, dJCal, rJCal32);
    k3Jr[ii] = dt * jupiter[np1t - 1][1][ii];
    k3mIov[ii] = dt * fIov(mSat, dJIo, rJIo32, dPIo, rPIo32, dIoCal, rIoCal32,
                           dIoEu, rIoEu32, dIoG, rIoG32);
    k3mIor[ii] = dt * moonIo[np1t - 1][1][ii];
    k3mCalv[ii] = dt * fCalv(mSat, dJCal, rJCal32, dPCal, rPCal32, dIoCal,
                             rIoCal32, dCalEu, rCalEu32, dCalG, rCalG32);
    k3mCalr[ii] = dt * moonCal[np1t - 1][1][ii];
    k3mEuv[ii] = dt * fEuv(mSat, dJEu, rJEu32, dPEu, rPEu32, dIoEu, rIoEu32,
                           dCalEu, rCalEu32, dEuG, rEuG32);
    k3mEur[ii] = dt * moonEu[np1t - 1][1][ii];
    k3mGv[ii] = dt * fGanv(mSat, dJG, rJG32, dPG, rPG32, dIoG, rIoG32, dCalG,
                           rCalG32, dEuG, rEuG32);
    k3mGr[ii] = dt * moonGan[np1t - 1][1][ii];
  }
  // Evaluated at final time already, so mass is one full step down
  if (mSat > mProbe) mSat += 0.5 * dt * dm;  // dm < 0
  // Set in between values for the 4th step
  ////////////////////////////////////////////////////////////
  // PROBE
  rJP32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k3Sr[0] -
                            jupiter[np1t - 1][0][0] - 0.5 * k3Jr[0],
                        2) +
                        pow(sat[np1t - 1][0][1] + 0.5 * k3Sr[1] -
                                jupiter[np1t - 1][0][1] - 0.5 * k3Jr[1],
                            2),
                    1.5);
  rPG32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k3Sr[0] -
                            moonGan[np1t - 1][0][0] - 0.5 * k3mGr[0],
                        2) +
                        pow(sat[np1t - 1][0][1] + 0.5 * k3Sr[1] -
                                moonGan[np1t - 1][0][1] - 0.5 * k3mGr[1],
                            2),
                    1.5);
  rPIo32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k3Sr[0] -
                             moonIo[np1t - 1][0][0] - 0.5 * k3mIor[0],
                         2) +
                         pow(sat[np1t - 1][0][1] + 0.5 * k3Sr[1] -
                                 moonIo[np1t - 1][0][1] - 0.5 * k3mIor[1],
                             2),
                     1.5);
  rPCal32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k3Sr[0] -
                              moonCal[np1t - 1][0][0] - 0.5 * k3mCalr[0],
                          2) +
                          pow(sat[np1t - 1][0][1] + 0.5 * k3Sr[1] -
                                  moonCal[np1t - 1][0][1] - 0.5 * k3mCalr[1],
                              2),
                      1.5);
  rPEu32 = 1.0 / pow(pow(sat[np1t - 1][0][0] + 0.5 * k3Sr[0] -
                             moonEu[np1t - 1][0][0] - 0.5 * k3mEur[0],
                         2) +
                         pow(sat[np1t - 1][0][1] + 0.5 * k3Sr[1] -
                                 moonEu[np1t - 1][0][1] - 0.5 * k3mEur[1],
                             2),
                     1.5);
  ////////////////////////////////////////////////////////////
  // JUPITER
  rJG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k3mGr[0] -
                            jupiter[np1t - 1][0][0] - 0.5 * k3Jr[0],
                        2) +
                        pow(moonGan[np1t - 1][0][1] + 0.5 * k3mGr[1] -
                                jupiter[np1t - 1][0][1] - 0.5 * k3Jr[1],
                            2),
                    1.5);
  rJIo32 = 1.0 / pow(pow(moonIo[np1t - 1][0][0] + 0.5 * k3mIor[0] -
                             jupiter[np1t - 1][0][0] - 0.5 * k3Jr[0],
                         2) +
                         pow(moonIo[np1t - 1][0][1] + 0.5 * k3mIor[1] -
                                 jupiter[np1t - 1][0][1] - 0.5 * k3Jr[1],
                             2),
                     1.5);
  rJCal32 = 1.0 / pow(pow(moonCal[np1t - 1][0][0] + 0.5 * k3mCalr[0] -
                              jupiter[np1t - 1][0][0] - 0.5 * k3Jr[0],
                          2) +
                          pow(moonCal[np1t - 1][0][1] + 0.5 * k3mCalr[1] -
                                  jupiter[np1t - 1][0][1] - 0.5 * k3Jr[1],
                              2),
                      1.5);
  rJEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k3mEur[0] -
                             jupiter[np1t - 1][0][0] - 0.5 * k3Jr[0],
                         2) +
                         pow(moonEu[np1t - 1][0][1] + 0.5 * k3mEur[1] -
                                 jupiter[np1t - 1][0][1] - 0.5 * k3Jr[1],
                             2),
                     1.5);
  ////////////////////////////////////////////////////////////
  // Io
  rIoG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k3mGr[0] -
                             moonIo[np1t - 1][0][0] - 0.5 * k3mIor[0],
                         2) +
                         pow(moonGan[np1t - 1][0][1] + 0.5 * k3mGr[1] -
                                 moonIo[np1t - 1][0][1] - 0.5 * k3mIor[1],
                             2),
                     1.5);
  rIoCal32 = 1.0 / pow(pow(moonCal[np1t - 1][0][0] + 0.5 * k3mCalr[0] -
                               moonIo[np1t - 1][0][0] - 0.5 * k3mIor[0],
                           2) +
                           pow(moonCal[np1t - 1][0][1] + 0.5 * k3mCalr[1] -
                                   moonIo[np1t - 1][0][1] - 0.5 * k3mIor[1],
                               2),
                       1.5);
  rIoEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k3mEur[0] -
                              moonIo[np1t - 1][0][0] - 0.5 * k3mIor[0],
                          2) +
                          pow(moonEu[np1t - 1][0][1] + 0.5 * k3mEur[1] -
                                  moonIo[np1t - 1][0][1] - 0.5 * k3mIor[1],
                              2),
                      1.5);
  ////////////////////////////////////////////////////////////
  // Cal
  rCalG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k3mGr[0] -
                              moonCal[np1t - 1][0][0] - 0.5 * k3mCalr[0],
                          2) +
                          pow(moonGan[np1t - 1][0][1] + 0.5 * k3mGr[1] -
                                  moonCal[np1t - 1][0][1] - 0.5 * k3mCalr[1],
                              2),
                      1.5);
  rCalEu32 = 1.0 / pow(pow(moonEu[np1t - 1][0][0] + 0.5 * k3mEur[0] -
                               moonCal[np1t - 1][0][0] - 0.5 * k3mCalr[0],
                           2) +
                           pow(moonEu[np1t - 1][0][1] + 0.5 * k3mEur[1] -
                                   moonCal[np1t - 1][0][1] - 0.5 * k3mCalr[1],
                               2),
                       1.5);
  ////////////////////////////////////////////////////////////
  // Europa
  rEuG32 = 1.0 / pow(pow(moonGan[np1t - 1][0][0] + 0.5 * k3mGr[0] -
                             moonEu[np1t - 1][0][0] - 0.5 * k3mEur[0],
                         2) +
                         pow(moonGan[np1t - 1][0][1] + 0.5 * k3mGr[1] -
                                 moonEu[np1t - 1][0][1] - 0.5 * k3mEur[1],
                             2),
                     1.5);

  vpNorm = sqrt(pow(sat[np1t - 1][1][0] + k3Sv[0], 2) +
                pow(sat[np1t - 1][1][1] + k3Sv[1], 2));
  Ve[0] = V_e * (sat[np1t - 1][1][0] + k3Sv[0]) / vpNorm;
  Ve[1] = V_e * (sat[np1t - 1][1][1] + k3Sv[1]) / vpNorm;
  // Do fourth RK step
  for (long int ii = 0; ii < ns; ++ii) {
    dJP = sat[np1t - 1][0][ii] + 0.5 * k3Sr[ii] - jupiter[np1t - 1][0][ii] -
          0.5 * k3Jr[ii];
    dPG = moonGan[np1t - 1][0][ii] + 0.5 * k3mGr[ii] - sat[np1t - 1][0][ii] -
          0.5 * k3Sr[ii];
    dPIo = moonIo[np1t - 1][0][ii] + 0.5 * k3mIor[ii] - sat[np1t - 1][0][ii] -
           0.5 * k3Sr[ii];
    dPCal = moonCal[np1t - 1][0][ii] + 0.5 * k3mCalr[ii] -
            sat[np1t - 1][0][ii] - 0.5 * k3Sr[ii];
    dPEu = moonEu[np1t - 1][0][ii] + 0.5 * k3mEur[ii] - sat[np1t - 1][0][ii] -
           0.5 * k3Sr[ii];

    dJG = moonGan[np1t - 1][0][ii] + 0.5 * k3mGr[ii] -
          jupiter[np1t - 1][0][ii] - 0.5 * k3Jr[ii];
    dJIo = moonIo[np1t - 1][0][ii] + 0.5 * k3mIor[ii] -
           jupiter[np1t - 1][0][ii] - 0.5 * k3Jr[ii];
    dJCal = moonCal[np1t - 1][0][ii] + 0.5 * k3mCalr[ii] -
            jupiter[np1t - 1][0][ii] - 0.5 * k3Jr[ii];
    dJEu = moonEu[np1t - 1][0][ii] + 0.5 * k3mEur[ii] -
           jupiter[np1t - 1][0][ii] - 0.5 * k3Jr[ii];

    dIoG = moonGan[np1t - 1][0][ii] + 0.5 * k3mGr[ii] -
           moonIo[np1t - 1][0][ii] - 0.5 * k3mIor[ii];
    dIoCal = moonCal[np1t - 1][0][ii] + 0.5 * k3mCalr[ii] -
             moonIo[np1t - 1][0][ii] - 0.5 * k3mIor[ii];
    dIoEu = moonEu[np1t - 1][0][ii] + 0.5 * k3mEur[ii] -
            moonIo[np1t - 1][0][ii] - 0.5 * k3mIor[ii];

    dCalG = moonGan[np1t - 1][0][ii] + 0.5 * k3mGr[ii] -
            moonCal[np1t - 1][0][ii] - 0.5 * k3mCalr[ii];
    dCalEu = moonEu[np1t - 1][0][ii] + 0.5 * k3mEur[ii] -
             moonCal[np1t - 1][0][ii] - 0.5 * k3mCalr[ii];

    dEuG = moonGan[np1t - 1][0][ii] + 0.5 * k3mGr[ii] -
           moonEu[np1t - 1][0][ii] - 0.5 * k3mEur[ii];

    k4Sv[ii] = dt * fSatv(dmt, mSat, dJP, rJP32, dPG, rPG32, dPIo, rPIo32,
                          dPCal, rPCal32, dPEu, rPEu32, Ve[ii]);
    k4Sr[ii] = dt * sat[np1t - 1][1][ii];
    k4Jv[ii] = dt * fJv(mSat, dJP, rJP32, dJG, rJG32, dJIo, rJIo32, dJEu,
                        rJEu32, dJCal, rJCal32);
    k4Jr[ii] = dt * jupiter[np1t - 1][1][ii];
    k4mIov[ii] = dt * fIov(mSat, dJIo, rJIo32, dPIo, rPIo32, dIoCal, rIoCal32,
                           dIoEu, rIoEu32, dIoG, rIoG32);
    k4mIor[ii] = dt * moonIo[np1t - 1][1][ii];
    k4mCalv[ii] = dt * fCalv(mSat, dJCal, rJCal32, dPCal, rPCal32, dIoCal,
                             rIoCal32, dCalEu, rCalEu32, dCalG, rCalG32);
    k4mCalr[ii] = dt * moonCal[np1t - 1][1][ii];
    k4mEuv[ii] = dt * fEuv(mSat, dJEu, rJEu32, dPEu, rPEu32, dIoEu, rIoEu32,
                           dCalEu, rCalEu32, dEuG, rEuG32);
    k4mEur[ii] = dt * moonEu[np1t - 1][1][ii];
    k4mGv[ii] = dt * fGanv(mSat, dJG, rJG32, dPG, rPG32, dIoG, rIoG32, dCalG,
                           rCalG32, dEuG, rEuG32);
    k4mGr[ii] = dt * moonGan[np1t - 1][1][ii];
  }

  // Combine RK steps
  for (long int ii = 0; ii < ns; ++ii) {
    sat[np1t][1][ii] = sat[np1t - 1][1][ii] +
                       a0 * (k1Sv[ii] + 2.0 * (k2Sv[ii] + k3Sv[ii]) + k4Sv[ii]);
    sat[np1t][0][ii] = sat[np1t - 1][0][ii] +
                       a0 * (k1Sr[ii] + 2.0 * (k2Sr[ii] + k3Sr[ii]) + k4Sr[ii]);
    jupiter[np1t][1][ii] =
        jupiter[np1t - 1][1][ii] +
        a0 * (k1Jv[ii] + 2.0 * (k2Jv[ii] + k3Jv[ii]) + k4Jv[ii]);
    jupiter[np1t][0][ii] =
        jupiter[np1t - 1][0][ii] +
        a0 * (k1Jr[ii] + 2.0 * (k2Jr[ii] + k3Jr[ii]) + k4Jr[ii]);
    moonIo[np1t][1][ii] =
        moonIo[np1t - 1][1][ii] +
        a0 * (k1mIov[ii] + 2.0 * (k2mIov[ii] + k3mIov[ii]) + k4mIov[ii]);
    moonIo[np1t][0][ii] =
        moonIo[np1t - 1][0][ii] +
        a0 * (k1mIor[ii] + 2.0 * (k2mIor[ii] + k3mIor[ii]) + k4mIor[ii]);
    moonCal[np1t][1][ii] =
        moonCal[np1t - 1][1][ii] +
        a0 * (k1mCalv[ii] + 2.0 * (k2mCalv[ii] + k3mCalv[ii]) + k4mCalv[ii]);
    moonCal[np1t][0][ii] =
        moonCal[np1t - 1][0][ii] +
        a0 * (k1mCalr[ii] + 2.0 * (k2mCalr[ii] + k3mCalr[ii]) + k4mCalr[ii]);
    moonEu[np1t][1][ii] =
        moonEu[np1t - 1][1][ii] +
        a0 * (k1mEuv[ii] + 2.0 * (k2mEuv[ii] + k3mEuv[ii]) + k4mEuv[ii]);
    moonEu[np1t][0][ii] =
        moonEu[np1t - 1][0][ii] +
        a0 * (k1mEur[ii] + 2.0 * (k2mEur[ii] + k3mEur[ii]) + k4mEur[ii]);
    moonGan[np1t][1][ii] =
        moonGan[np1t - 1][1][ii] +
        a0 * (k1mGv[ii] + 2.0 * (k2mGv[ii] + k3mGv[ii]) + k4mGv[ii]);
    moonGan[np1t][0][ii] =
        moonGan[np1t - 1][0][ii] +
        a0 * (k1mGr[ii] + 2.0 * (k2mGr[ii] + k3mGr[ii]) + k4mGr[ii]);
  }
  return;
}
// RHS Probe
double fSatv(double dmt, double mSt, double dJP, double rJP32, double dPG,
             double rPG32, double dPIo, double rPIo32, double dPCal,
             double rPCal32, double dPEu, double rPEu32, double Ve) {
  return dmt / mSt * Ve - G * mJ * rJP32 * dJP + G * mGan * rPG32 * dPG +
         G * mIo * rPIo32 * dPIo + G * mEur * rPEu32 * dPEu +
         G * mCal * rPCal32 * dPCal;
}
// RHS Jupiter velocity
double fJv(double mSt, double dJP, double rJP32, double dJG, double rJG32,
           double dJIo, double rJIo32, double dJEu, double rJEu32, double dJCal,
           double rJCal32) {
  return G * mSt * rJP32 * dJP + G * mGan * rJG32 * dJG +
         G * mIo * rJIo32 * dJIo + G * mEur * rJEu32 * dJEu +
         G * mCal * rJCal32 * dJCal;
}
// RHS moon Io
double fIov(double mSt, double dJIo, double rJIo32, double dPIo, double rPIo32,
            double dIoCal, double rIoCal32, double dIoEu, double rIoEu32,
            double dIoG, double rIoG32) {
  return -G * mJ * rJIo32 * dJIo - G * mSt * rPIo32 * dPIo +
         G * mIo * rIoG32 * dIoG + G * mCal * rIoCal32 * dIoCal +
         G * mEur * rIoEu32 * dIoEu;
}
// RHS moon Callisto
double fCalv(double mSt, double dJCal, double rJCal32, double dPCal,
             double rPCal32, double dIoCal, double rIoCal32, double dCalEu,
             double rCalEu32, double dCalG, double rCalG32) {
  return -G * mJ * rJCal32 * dJCal - G * mSt * rPCal32 * dPCal -
         G * mIo * rIoCal32 * dIoCal + G * mGan * rCalG32 * dCalG +
         G * mEur * rCalEu32 * dCalEu;
}
// RHS moon Europa
double fEuv(double mSt, double dJEu, double rJEu32, double dPEu, double rPEu32,
            double dIoEu, double rIoEu32, double dCalEu, double rCalEu32,
            double dEuG, double rEuG32) {
  return -G * mJ * rJEu32 * dJEu - G * mSt * rPEu32 * dPEu -
         G * mIo * rIoEu32 * dIoEu - G * mCal * rCalEu32 * dCalEu +
         G * mGan * rEuG32 * dEuG;
}
// RHS moon Ganymede
double fGanv(double mSt, double dJG, double rJG32, double dPG, double rPG32,
             double dIoG, double rIoG32, double dCalG, double rCalG32,
             double dEuG, double rEuG32) {
  return -G * mJ * rJG32 * dJG - G * mSt * rPG32 * dPG -
         G * mIo * rIoG32 * dIoG - G * mCal * rCalG32 * dCalG -
         G * mEur * rEuG32 * dEuG;
}

// Check if any of the objects have crashed long into one another, returns
// 1 if crashed, 0 if not
long int checkCrash(long int ii) {
  int crashYet = 0;
  // Jupiter and Satellite, satellite is polong int particle
  double sep = sqrt(pow(sat[ii][0][0] - jupiter[ii][0][0], 2) +
                    pow(sat[ii][0][1] - jupiter[ii][0][1], 2));
  if (sep < rJ) {
    printf(
        "\nSatellite crashed into Jupiter at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Jupiter is %lg\n", sep,
           rJ);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if probe crashed into Ganymede
  sep = sqrt(pow(sat[ii][0][0] - moonGan[ii][0][0], 2) +
             pow(sat[ii][0][1] - moonGan[ii][0][1], 2));
  if (sep < rGan) {
    printf(
        "\nSatellite crashed into Ganymede at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Ganymede is %lg\n", sep,
           rGan);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if probe crashed into Callisto
  sep = sqrt(pow(sat[ii][0][0] - moonCal[ii][0][0], 2) +
             pow(sat[ii][0][1] - moonCal[ii][0][1], 2));
  if (sep < rCal) {
    printf(
        "\nSatellite crashed into Callisto at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Callisto is %lg\n", sep,
           rCal);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if probe crashed into Io
  sep = sqrt(pow(sat[ii][0][0] - moonIo[ii][0][0], 2) +
             pow(sat[ii][0][1] - moonIo[ii][0][1], 2));
  if (sep < rCal) {
    printf(
        "\nSatellite crashed into Io at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Io is %lg\n", sep, rIo);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if probe crashed into Europa
  sep = sqrt(pow(sat[ii][0][0] - moonEu[ii][0][0], 2) +
             pow(sat[ii][0][1] - moonEu[ii][0][1], 2));
  if (sep < rCal) {
    printf(
        "\nSatellite crashed into Europa at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Europa is %lg\n", sep,
           rEu);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if Io crashed into Jupiter
  sep = sqrt(pow(jupiter[ii][0][0] - moonIo[ii][0][0], 2) +
             pow(jupiter[ii][0][1] - moonIo[ii][0][1], 2));
  if (sep < rJ + rIo) {
    printf(
        "\nIo crashed into Jupiter at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Jupiter is %lg\n", sep,
           rJ);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if Ganymede crashed into Jupiter
  sep = sqrt(pow(jupiter[ii][0][0] - moonGan[ii][0][0], 2) +
             pow(jupiter[ii][0][1] - moonGan[ii][0][1], 2));
  if (sep < rJ + rGan) {
    printf(
        "\nGanymede crashed into Jupiter at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Jupiter is %lg\n", sep,
           rJ);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if Callisto crashed into Jupiter
  sep = sqrt(pow(jupiter[ii][0][0] - moonCal[ii][0][0], 2) +
             pow(jupiter[ii][0][1] - moonCal[ii][0][1], 2));
  if (sep < rJ + rCal) {
    printf(
        "\nCallisto crashed into Jupiter at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radius of Jupiter is %lg\n", sep,
           rJ);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if Io crashed into Ganymede
  sep = sqrt(pow(moonGan[ii][0][0] - moonIo[ii][0][0], 2) +
             pow(moonGan[ii][0][1] - moonIo[ii][0][1], 2));
  if (sep < rGan + rIo) {
    printf(
        "\nIo crashed into Ganymede at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radial separation is %lg\n", sep,
           rGan + rIo);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if Io crashed into Callisto
  sep = sqrt(pow(moonCal[ii][0][0] - moonIo[ii][0][0], 2) +
             pow(moonCal[ii][0][1] - moonIo[ii][0][1], 2));
  if (sep < rCal + rIo) {
    printf(
        "\nIo crashed into Callisto at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radial separation is %lg\n", sep,
           rCal + rIo);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  // Check if Callisto crashed into Ganymede
  sep = sqrt(pow(moonGan[ii][0][0] - moonCal[ii][0][0], 2) +
             pow(moonGan[ii][0][1] - moonCal[ii][0][1], 2));
  if (sep < rGan + rCal) {
    printf(
        "\nCallisto crashed into Ganymede at time %lg, "
        "integer %ld.\n",
        ii * dt, ii);
    printf("Radial separation is: %lg, and radial separation is %lg\n", sep,
           rGan + rCal);
    printInitialData(ii);
    nt = ii;  // Only prlong int to crash time
    crashYet = 1;
  }
  if (crashYet == 1) return crashYet;
  return 0;
}

// ND This is used to check if we are using OpenGL. Sets nt=1
void checkAnim() { return; }
