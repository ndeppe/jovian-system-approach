// Global variables are REQUIRED for OpenGL to work as it does
// this is why they are used, even though in general it is really
// awful practice to use them.
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include "2Dsolver.hpp"
#ifdef __APPLE__
#include <OpenGL/OpenGl.h>
// #define __gl_h_
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

// OpenGL needs these for error handling.
GLenum errCode;
const GLubyte* errString;
// The scale factor by which to make everything smaller
double scale = 40000;
// How far back to zoom everything
double zoomBack = -130;
// Translate the drawings, not text
// double globalShift[3] = {-12.0,-4.0,0.0};
double globalShift[3] = {-0.0, -4.0, 0.0};
// By how much bigger to make moons than they really are
double moonScale = 8.0;
// ND The unscale size of the probe
double shipSize = 64000.0;
// How quickly (as in how many time steps to take per update)
// the animation should go.
int oSpeed = 10;
// Used to check if we have crashed
int crashed(0);

// Called when a key is pressed
void handleKeypress(unsigned char key, int x __attribute__((unused)),
                    int y __attribute__((unused))) {
  switch (key) {
    case 27:  // Escape key
      exit(0);
      break;
    case 113:  // q key
      exit(0);
      break;
    case 102:  // f key
      speed += 2;
      break;
    case 115:  // s key
      if (speed > 2) speed -= 2;
      break;
    case 32:  // space bar key
      if (speed > 0) {
        oSpeed = speed;
        speed = 0;
      } else
        speed = oSpeed;
      break;
      // case 114: // r key
      //   readData("t");
      //   break;
  }
}

// This prints a 2D text using bitmap characters
// in the given font, and the array of characters
// is output. Pass a string.c_str() array.
void print_bitmap_string(void* font, const char* s) {
  if (s && strlen(s)) {
    while (*s) {
      glutBitmapCharacter(font, *s);
      s++;
    }
  }
}

// This calculates the size of the string "s" as an integer value.
// This can then be used to calculate the centering value needed for the
// string.
int bitmapSize(void* font __attribute__((unused)), const char* s) {
  int size = 0;
  if (s && strlen(s)) {
    while (*s) {
      size += glutBitmapWidth(GLUT_BITMAP_TIMES_ROMAN_24, *s);
      s++;
    }
  }
  return size;
}

// Initializes 3D rendering
void initRendering() {
  GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
  GLfloat mat_shininess[] = {50.0};
  GLfloat light_ambient[] = {0.0, 0.0, 0.0, 1.0};
  GLfloat light_diffuse[] = {1.0, 1.0, 1.0, 1.0};
  GLfloat light_specular[] = {1.0, 1.0, 1.0, 1.0};
  GLfloat light_position[] = {-1.0, 1.0, 1.0, 0.0};
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glShadeModel(GL_SMOOTH);

  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);

  // glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

// Called when the window is resized
void handleResize(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (double)w / (double)h, 1.0, 200.0);
}

// Draws the 3D scene
void drawScene() {
  glEnableClientState(GL_VERTEX_ARRAY);  // Enable arrays
  glEnableClientState(GL_COLOR_ARRAY);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // Clear buffer

  glMatrixMode(GL_MODELVIEW);  // Switch to the drawing perspective
  glLoadIdentity();  // Reset the drawing perspective
  // double reShift = 5.0;
  // glTranslated(globalShift[0]/reShift,
  // 	       globalShift[1]/reShift, globalShift[2]/reShift);
  // Save the transformations performed thus far
  glPushMatrix();
  glTranslated(globalShift[0], globalShift[1], globalShift[2]);

  GLUquadric* quad;
  quad = gluNewQuadric();
  gluQuadricDrawStyle(quad, GLU_FILL);
  gluQuadricNormals(quad, GLU_NONE);

  // Draw Jupiter, but rescaled
  glTranslated(0., 0., zoomBack);
  glColor3f(1., 1., 0.1);
  gluDisk(quad, 0., rJ / scale, 50, 20);
  // Draw Io, but also rescaled
  glPopMatrix();
  glPushMatrix();
  glTranslated(globalShift[0], globalShift[1], globalShift[2]);
  glTranslated(moonIo[0][0][0] / scale, moonIo[0][0][1] / scale, zoomBack);
  glColor3d(1., 0., 0.1);
  gluDisk(quad, 0., moonScale * rIo / scale, 50, 20);

  // Draw Ganymede, but also rescaled
  glPopMatrix();
  glPushMatrix();
  glTranslated(globalShift[0], globalShift[1], globalShift[2]);
  glTranslatef(moonGan[0][0][0] / scale, moonGan[0][0][1] / scale, zoomBack);
  glColor3d(0., 0., 1.0);
  gluDisk(quad, 0., moonScale * rGan / scale, 50, 20);

  // Draw Callisto, but also rescaled
  glPopMatrix();
  glPushMatrix();
  glTranslated(globalShift[0], globalShift[1], globalShift[2]);
  glTranslated(moonCal[0][0][0] / scale, moonCal[0][0][1] / scale, zoomBack);
  glColor3d(0., 1., 1.0);
  gluDisk(quad, 0., moonScale * rCal / scale, 50, 20);

  // Draw Europa, but also rescaled
  glPopMatrix();
  glPushMatrix();
  glTranslated(globalShift[0], globalShift[1], globalShift[2]);
  glTranslated(moonEu[0][0][0] / scale, moonEu[0][0][1] / scale, zoomBack);
  glColor3d(0.1, 0.6, 0.7);
  gluDisk(quad, 0., moonScale * rEu / scale, 50, 20);

  // Draw Probe, but also rescaled
  glPopMatrix();
  glPushMatrix();
  glTranslated(globalShift[0], globalShift[1], globalShift[2]);
  glTranslated(sat[0][0][0] / scale, sat[0][0][1] / scale, zoomBack);
  // ND How many degrees to rotate, depends on the velocity vector
  double deg(0.0);
  if (sat[0][1][0] < 0.0 && sat[0][1][1] > 0.0)
    deg = -atan(sat[0][1][0] / sat[0][1][1]) / PI * 180;
  else if (sat[0][1][0] < 0.0 && sat[0][1][1] < 0.0)
    deg = 180 - atan(sat[0][1][0] / sat[0][1][1]) / PI * 180;
  else if (sat[0][1][0] > 0.0 && sat[0][1][1] < 0.0)
    deg = 180 - atan(sat[0][1][0] / sat[0][1][1]) / PI * 180;
  else if (sat[0][1][0] > 0.0 && sat[0][1][1] > 0.0)
    deg = -atan(sat[0][1][0] / sat[0][1][1]) / PI * 180;
  glRotated(deg, 0.0, 0.0, 1.0);
  glColor3d(0., 1., 0.0);
  // Draw a triangle aimed in the direction of the satellite.
  glBegin(GL_TRIANGLES);
  glVertex3d(0.0, shipSize / scale, 0.0);
  glVertex3d(0.5 * shipSize / scale, 0.0, 0.0);
  glVertex3d(-0.5 * shipSize / scale, 0.0, 0.0);
  glEnd();

  // Draw the fuel bar at the top left.
  glPopMatrix();
  glPushMatrix();
  drawFuel();

  // Draw an X on the crash site...
  if (crashed) drawCrashSite();

  // Draw the current time
  printTime();

  // Done drawing
  glPopMatrix();

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  if ((errCode = glGetError()) != GL_NO_ERROR) {
    errString = gluErrorString(errCode);
    std::cerr << "OpenGL error: " << errString << std::endl;
  }
  glutSwapBuffers();  // Use for double or more buffers
  gluDeleteQuadric(quad);
}

void update(int value __attribute__((unused))) {
  for (int ii = 0; ii < speed; ++ii) {
    tstep(0);
    crashed = checkCrash(0);
    cTime++;
    if (crashed) {
      oSpeed = speed;
      speed = 0;
    }
  }
  // Tell GLUT that the display has changed
  glutPostRedisplay();
  // Tell GLUT to call update again in updateTime milliseconds
  glutTimerFunc(1, update, 0);
}

// This draws the background lines for the time progress indicator.
void drawFuel() {
  double fuel = -4.0 + 4.0 * (mSat - mProbe) / mFuel;
  double vTop = 4, vBottom = 3.5, vLeft = -4, vRight = 0.0;
  // ND Draw the left and right vertical bars, in that order
  glBegin(GL_QUADS);
  glColor4f(1.0, 1.0, 1.0, 0.5);
  glVertex3f(vLeft - 0.05, vTop, -10);
  glVertex3f(vLeft - 0.05, vBottom, -10);
  glVertex3f(vLeft, vBottom, -10);
  glVertex3f(vLeft, vTop, -10);
  glEnd();

  glBegin(GL_QUADS);
  glVertex3f(vRight, vTop, -10);
  glVertex3f(vRight, vBottom, -10);
  glVertex3f(vRight + 0.05, vBottom, -10);
  glVertex3f(vRight + 0.05, vTop, -10);
  glEnd();

  // Draw the center, top and bottom white horizontal bars,
  // respectively
  glBegin(GL_QUADS);
  glVertex3f(vLeft, vTop, -10);
  glVertex3f(vLeft, vTop - 0.05, -10);
  glVertex3f(vRight, vTop - 0.05, -10);
  glVertex3f(vRight, vTop, -10);
  glEnd();

  glBegin(GL_QUADS);
  glVertex3f(vLeft, vBottom, -10);
  glVertex3f(vLeft, vBottom + 0.05, -10);
  glVertex3f(vRight, vBottom + 0.05, -10);
  glVertex3f(vRight, vBottom, -10);
  glEnd();

  // Fill the fuel bar with colour
  glBegin(GL_QUADS);
  glColor4f(0.0, 0.3, 1, 0.8);
  glVertex3f(vLeft, vTop - 0.05, -10);
  glVertex3f(vLeft, vBottom + 0.05, -10);
  glVertex3f(fuel, vBottom + 0.05, -10);
  glVertex3f(fuel, vTop - 0.05, -10);
  glEnd();

  // Write the fuel percentage and kilograms left
  char cfuel[250];
  double fuelPer = 100.0 * (mSat - mProbe) / mFuel;
  double fuelKG = (mSat - mProbe);
  sprintf(cfuel, "Fuel Left: %3.1f%%, %5.0f kg", fuelPer, fuelKG);
  glColor3f(1.0, 1.0, 1.0);
  glRasterPos3f(vLeft + 0.2, vTop - 0.4, -9.8);
  print_bitmap_string(GLUT_BITMAP_TIMES_ROMAN_24, cfuel);
  return;
}

// Mark the crash site. This assumes it is the probe that crashed,
// not one of the moons.
void drawCrashSite() {
  glPopMatrix();
  glPushMatrix();
  glTranslated(sat[0][0][0] / scale, sat[0][0][1] / scale, zoomBack);
  printf("Crashed!\n");

  glColor3f(1.0, 0.0, 0.0);
  glRasterPos3f(-1.0, -1.0, 1.8);
  print_bitmap_string(GLUT_BITMAP_TIMES_ROMAN_24, "X");

  return;
}

// Write out the current time that the animation is on
void printTime() {
  glPopMatrix();
  glPushMatrix();
  char time[180];

  sprintf(time, "Time: %.1f days", cTime * dt / 86400.);
  glColor3f(1.0, 1.0, 1.0);
  glRasterPos3f(1.2, 3.6, -9.8);
  print_bitmap_string(GLUT_BITMAP_TIMES_ROMAN_24, time);

  return;
}
