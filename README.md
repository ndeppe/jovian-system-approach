Jovian System Approach
======================

What is it?
-----------

The Jovian System Approach code was originally written for our
team in the [University Physics Competition](http://www.uphysicsc.com/).
We tackled the problem of how much
fuel could be saved by using one of Jupiter's moons as a
gravitational slingshot to slow a probe down into orbit. A detailed
description of the question can be found
[here](http://www.uphysicsc.com/2013contest.html).

The code solves Newton's law, F=ma, for the Jupiter, probe,
Ganymede, Europa, Io and Callisto system. The code
used in the competition only had one moon for simplicity.

Requirements
------------

Windows:
The code has not been tested on any version of Windows, but you
may try it if you wish.

Linux or *NIX:
You will need OpenGL, GLU and GLUT installed as well as GCC.
For Ubuntu anything 12.xx or newer should be fine. Confirmed
working on 14.04.

Mac OSX:
Require 10.7 or newer, though older versions may work. You will
need the Apple Command Line Tools to compile the code. This can
be downloaded either via Xcode or from developer.apple.com/downloads.
Download and install whichever is the most recent for your version
of OSX.

Installation
------------

Find a folder where you want the jovian-system-approach sub-directory
to be placed. Then open a Terminal and `cd` to that folder. Now run
`git clone https://ndeppe@bitbucket.org/ndeppe/jovian-system-approach.git`
Once the clone is done you run `cd jovian-system-approach`. To compile,
simply run `make`.

Usage
-----

The original version of this code had an even more terrible user
interface than the current. I will discuss the current implementation
in its two distinct components: 1) rendering and 2) input files.
You can run either `probeAnim` to see a live render of the physics
or `probeData` to have it write data files that can be visualized
using [ROOT](http://root.cern.ch/). I recommend just sticking to
`probeAnim` for simplicity and fun.

1) Rendering:
During render you see the time in days that the simulation has run
and the amount of the initial fuel in the rocket that has been used.
The fuel is burnt in such a manner as to slow the craft down. That
is, the thrusters are pointing forwards. You can use certain keys
to adjust things:
f: increase speed of animation
s: decrease speed of animation
space: pause/unpause animation
q: quit animation
esc: quit animation

2) Input File:
The input file(s) allow you to specify a certain physical setup
that will be evolved. You can have multiple and pass the name
of the one you want to use to the code at runtime. This is done
by running the code using:
`
./probeAnim inputFileName.txt
`
There is a sample input file provided that also explains what each
parameter does.

To run the program first cd to the jovian-system-approach directory.
To use the provided input file type
`
./probeAnim inputFileName.txt
`
in the same terminal session. You should have a window pop
up animating the evolution


Known Bugs
----------

GLUT and GLU are deprecated on OSX since 10.9. We are aware of this
but are refusing to move to something else until support is lost
completely. Currently the code still compiles and runs correctly
so we will wait to change.

Apologies
---------

I apologize for the terrible coding style and formatting. The code was
written for the University Physics Competition
and time was short. I agree, it should be revamped and more comments
need to be added for clarity, however I am only one person and am
doing too many things at once. This code is not a high priority.

Licensing
---------

Please see the file called LICENSE.

Contacts
--------

If you find any bugs or issues feel free to either make a fix
for them and use a Pull Request or open an issue in the Issue
Tracker. To contact the developer(s) please open an issue
with the name "Contact Request" and provide a method of
contacting you.

Credits
-------

The equations and physical understanding were worked out by
Jared Enns, Nick Reid and Nils Deppe. The code was written
by Nils Deppe. Analysis of the data for the Competition was
done by Jared Enns, Nick Reid and Nils Deppe. This work was
supported by [The University of Winnipeg Physics Department](http://www.uwinnipeg.ca/physics/).
Jared Enns, Nick Reid and Nils Deppe thank Andrew Frey for
training and advice prior to the competition.